osearch() {
    sudo opkg find "*$1*"
}

alias oadd='sudo opkg install'
alias oremove='sudo opkg remove'
alias oup='sudo opkg update'

## LEDE tmux package lack some functions...
alias tput=""
alias tty="hostname"
