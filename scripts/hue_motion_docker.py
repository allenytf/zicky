#!/usr/bin/env python3

import sys
import argparse
import logging
import logging.handlers
import time
import socket
import datetime as dt
import requests
from gpiozero import MotionSensor
from astral import Astral

def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--bridge',
        required=True,
        action='store', dest='BRIDGE_URL')

    parser.add_argument(
        '--username',
        required=True,
        action='store', dest='HUE_USERNAME')

    parser.add_argument(
        '--light',
        required=True,
        action='store', dest='LIGHT_NAME')

    result = parser.parse_args()
    return result

class Light(object):
    def __init__(self, light_name):
        self.name = light_name
        self.is_on = False
        self.brightness = 0
        self.color_temp = 233
        self.full_bri = 254
        self.full_ct = 233
        self.dimmed_bri = 127
        self.dimmed_ct = 233


class HueMotion(object):

    def __init__(self, bridge_url, hue_username):
        self.stdin_path = "/dev/null"
        self.stdout_path = "/dev/tty"
        self.stderr_path = "/dev/tty"
        self.pidfile_path = "/var/run/hue_motion.pid"
        self.pidfile_timeout = 5
        self.no_move_threshold_sec = 1800    # in seconds
        self.scan_freq = 0.5    # in seconds
        self.no_move_threshold = self.no_move_threshold_sec / self.scan_freq
        self.no_move_count_down = self.no_move_threshold
        self.reset_hue_connection(bridge_url, hue_username)

    def reset_hue_connection(self, bridge_url, hue_username):
        self.hue_bridge_ip = socket.gethostbyname(bridge_url)
        self.user_key = hue_username
        self.hue_url = "http://{}/api/{}".format(self.hue_bridge_ip, self.user_key)
        self.light_list = {
            "living": "3",
            "room": "5",
            "allen": "8"
            }

    def commit_state_change(self, the_light, cmd):
        requests.put("{}/lights/{}/state".format(self.hue_url, self.light_list[the_light.name]), json=cmd)

    def check_light(self, the_light):
        resp = requests.get("{}/lights/{}".format(self.hue_url, self.light_list[the_light.name]))
        resp_json = resp.json()
        return resp_json["state"]

    def count_down(self, the_light):
        self.no_move_count_down -= 1
        LOGGER.debug("counting down " + str(self.no_move_count_down))
        if self.no_move_count_down == self.no_move_threshold / 2:
            self.light_dimm(the_light)
        if self.no_move_count_down == 0:
            self.light_off(the_light)
            self.reset_count()

    def light_on(self, the_light):
        if the_light.name == "allen":
            cmd = {
                "on": True,
                "bri": the_light.full_bri,
                "ct": the_light.full_ct
                }
        else:
            cmd = {
                "on": True,
                "bri": the_light.full_bri
                }
        if not the_light.is_on or (the_light.is_on and the_light.brightness != the_light.full_bri):
            LOGGER.info("Turning on " + the_light.name)
            self.commit_state_change(the_light, cmd)

    def light_dimm(self, the_light):
        if the_light.name == "allen":
            cmd = {
                "on": True,
                "bri": the_light.dimmed_bri,
                "ct": the_light.dimmed_ct
                }
        else:
            cmd = {
                "on": True,
                "bri": the_light.dimmed_bri
                }
        if the_light.is_on and the_light.brightness == the_light.full_bri:
            LOGGER.info("Dimming " + the_light.name)
            self.commit_state_change(the_light, cmd)

    def light_off(self, the_light):
        cmd = {
            "on": False
            }
        if the_light.is_on:
            LOGGER.info("Turning off " + the_light.name)
            self.commit_state_change(the_light, cmd)

    def reset_count(self):
        LOGGER.debug("resetting no_motion_count")
        self.no_move_count_down = self.no_move_threshold

    def run(self):
        LOGGER.info("Starting hue_motion daemon")

        pir = MotionSensor(4)
        the_light = Light(LIGHT_NAME)

        time_delta = dt.timedelta(minutes=30)
        astral = Astral()
        astral.solar_depression = "civil"
        city_name = "San Francisco"
        city = astral[city_name]
        sun = city.sun(date=dt.date.today(), local=True)
        stime = sun["sunset"].time()
        start_time = (dt.datetime.combine(dt.date(1, 1, 1), stime) - time_delta).time()
        LOGGER.info("Updating start time to " + str(start_time))

        self.reset_count()

        while True:
            #update sunset/start time in the first second everyday
            if dt.datetime.now().time() <= dt.datetime(1, 1, 1, 0, 0, 0, 500000).time():
                sun = city.sun(date=dt.date.today(), local=True)
                stime = sun["sunset"].time()
                start_time = (dt.datetime.combine(dt.date(1, 1, 1), stime) - time_delta).time()
                LOGGER.info("Updating start time to " + str(start_time))
            if dt.datetime.now().time() >= start_time or the_light.is_on:
                try:
                    light_state = self.check_light(the_light)
                except requests.exceptions.ConnectionError as err:
                    LOGGER.error("Error: %s", err)
                    time.sleep(5)
                    LOGGER.info("Resetting Hue bridge connection...")
                    self.reset_hue_connection(ARGS.BRIDGE_URL, ARGS.HUE_USERNAME)
                    continue
                the_light.brightness = light_state["bri"]
                the_light.is_on = light_state["on"]
                if pir.motion_detected:
                    self.reset_count()
                    self.light_on(the_light)
                elif the_light.is_on:
                    self.count_down(the_light)
            time.sleep(self.scan_freq)


def init_logger():
    formatter = logging.Formatter("%(asctime)s - %(message)s")
    handler = logging.StreamHandler(sys.stdout) # log to stdout for docker
    handler.setLevel(logging.INFO)
    handler.setFormatter(formatter)
    logging.getLogger().addHandler(handler)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    return handler, logger


if __name__ == "__main__":
    ARGS = parse_args()
    LIGHT_NAME = ARGS.LIGHT_NAME
    HANDLER, LOGGER = init_logger()

    HueMotion(ARGS.BRIDGE_URL, ARGS.HUE_USERNAME).run()
